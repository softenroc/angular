import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Client } from '../models/client';
import { CLIENTS } from './client.json';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private httpClient:HttpClient) { }

  getClients():Observable<Client>{
    //return of(CLIENTS)
    return this.httpClient.get('https://pokeapi.co/api/v2/pokemon/charizard').pipe(map(response => response as Client)) ;
  }
}
