export class Client {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;    
}

export class Factura {
    id:number;
    clientes: Client[];
}